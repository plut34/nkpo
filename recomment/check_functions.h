#ifndef CHECK_FUNCTIONS_H
#define CHECK_FUNCTIONS_H
#include "constants.h"

/*!
 * \brief check_arguments функция проверки аргументов командной строки
 * \param [in] argc - число аргументов
 * \param [in] argv - список аргументов
 * \return QVector<ErrorType> - типы ошибкок
 */
QVector<ErrorType> check_arguments(int argc, char* argv[]);

/*!
 * \brief print_error функция вывода ошибки пользователю
 * \param [in] error - тип ошибки
 */
void print_error(ErrorType error);

/*!
 * \brief print_lines_error функция вывода ошибки пользователю с указанием строки в которой содержится ошибка
 * \param [in] error - тип ошибки и строка в которой содержится ошибка
 */
void print_lines_error(Error error);

#endif // CHECK_FUNCTIONS_H
