#ifndef TEST_FOR_FIND_UNMACHED_COMMENTS_H
#define TEST_FOR_FIND_UNMACHED_COMMENTS_H
#pragma once

#include <QObject>
#include <QTest>
#include <QMap>
#include <QString>
#include <QList>
#include <QDebug>
#include <workwithfile.h>
#include "constants.h"

class test_for_find_unmached_comments : public QObject
{
    Q_OBJECT
public:
    explicit test_for_find_unmached_comments(QObject *parent = 0);

signals:

public slots:

private Q_SLOTS:
    void test_data();
    void test();
};

#endif // TEST_FOR_FIND_UNMACHED_COMMENTS_H
