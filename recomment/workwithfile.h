#ifndef WORKWITHFILE_H
#define WORKWITHFILE_H
#include "constants.h"

/*!
 * \brief search_comments функция производящая выделение позиций многострочных комментариев
 * \param[in] text - строка содержащая исходный код из входного файла
 * \param[out] comment_locations - позиции начал и концов многострочных комментариев
 */
void search_comments(QString text, QVector <QPair<int, int> > &comment_locations);

/*!
 * \brief delete_comments функция производящая удаление многострочных комментариев в указанных позициях
 * \param[in|out] text - строка содержащая исходный код из входного файла
 * \param[in] comment_locations - позиции начал и концов многострочных комментариев
 */
void delete_comments(QString &text, QVector <QPair<int, int> > comment_locations);

/*!
 * \brief find_unmached_comments функция производящая поиск ошибок во входном файле
 * \param[in] text - строка содержащая исходный код из входного файла
 * \return QVector<Error> - тип ошибки во входном файле и указание строки в которой произошла ошибка
 */
QVector<Error> find_unmached_comments(QString text);

#endif // WORKWITHFILE_H
