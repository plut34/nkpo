#include "test_for_find_unmached_comments.h"

typedef QVector<Error> ERRORVector;
Q_DECLARE_METATYPE(ERRORVector)
test_for_find_unmached_comments::test_for_find_unmached_comments(QObject *parent) : QObject(parent)
{

}

void test_for_find_unmached_comments::test_data()
{
    //QVector<Error> find_unmached_comments(QString text)
    QString text;
    QVector<Error>  result;


    QTest::addColumn< QString > ("text");
    QTest::addColumn<QVector<Error> >("result");

    text = "/*\nФункция производящая сложение двух чисел\nint sum(int a, int b)\n{\nreturn a+b;\n}\n";
    Error newError;
    newError.Type = unmatched_opened_comment;
    newError.linenumber = 1 ;

    result.append(newError);

    QTest::newRow("Один многострочный комментарий в тексте программы") << text <<result;

    text.clear();
    result.clear();

    text = "/*\nФункция производящая сложение двух чисел//*/\nint sum(int a, int b)\n{\nreturn a+b;\n}\n";
    newError.Type = unmatched_opened_comment;
    newError.linenumber = 1 ;

    result.append(newError);

    QTest::newRow("В коде программы есть начало комментария, а его конец находится внутри однострочного комментария") << text <<result;

    text.clear();
    result.clear();

    text = "int sum(int a, int b)\n{\nВернуть результат*/\nreturn a+b;\n}\n";
    newError.Type = unmatched_closed_comment;
    newError.linenumber = 3 ;

    result.append(newError);

    QTest::newRow("В коде программы есть только конец многострочного комментария") << text <<result;

    text.clear();
    result.clear();

    text = "int sum(int a, int b)\n{\n// /*Вернуть результат\n*/\nreturn a+b;\n}\n";
    newError.Type = unmatched_closed_comment;
    newError.linenumber = 4 ;

    result.append(newError);

    QTest::newRow("В коде программы начало комментария находится внутри однострочного комментария, а конец не закомментирован") << text <<result;


    text.clear();
    result.clear();

    text = "/*\nФункция производящая сложение двух чисел\n*/\nint sum(int a, int b)\n{\nВернуть результат*/\nreturn a+b;\n}\n";
    newError.Type = unmatched_closed_comment;
    newError.linenumber = 6 ;

    result.append(newError);

    QTest::newRow("В коде программы есть полный комментарий и только конец другого комментария") << text <<result;

    text.clear();
    result.clear();

    text = "/*\nФункция производящая сложение двух чисел\n*/\nint sum(int a, int b)\n{\n/*Вернуть результат\nreturn a+b;\n}\n";
    newError.Type = unmatched_opened_comment;
    newError.linenumber = 6 ;

    result.append(newError);

    QTest::newRow("В коде программы есть полный комментарий и только начало другого комментария") << text <<result;


}

void test_for_find_unmached_comments::test()
{


    QVector<Error>  result_function;
    QFETCH(QString, text);
    QFETCH(QVector<Error>, result);

    result_function = find_unmached_comments( text);



    QVERIFY2(result_function == result, "INVALID RESULT");
}

