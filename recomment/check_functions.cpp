#include "check_functions.h"

QVector <ErrorType> check_arguments(int argc, char* argv[])
{
    QVector< ErrorType > errors;
    if (argc > 3 || argc < 3){
        errors.append(wrong_count_of_arg);
        return errors ;
    }
    if( argc == 3)
    {
       QString first_argument;
       QString second_argument;
       first_argument.append(argv[1]);
       second_argument.append(argv[2]);
       if(!first_argument.contains(".cpp"))
       {
           errors.append(not_in_arg);
           return errors;
       }
       if(!second_argument.contains(".cpp"))
       {
           errors.append(not_out_arg);
           return errors;
       }
    }

    return errors;
}



void print_error(ErrorType error)
{
    switch (error) {
    case wrong_count_of_arg:
        cout<<"Programm gets wrong count of arguments! Please check your arguments ";
        break;
    case not_in_arg:
        cout<<"Input file is not .cpp type";
        break;
    case not_out_arg:
        cout<<"Output file is not .cpp type";
        break;
    case cannot_read_file:
        cout<<"Programm cannot read file";
        break;
    case file_dont_exists:
        cout<<"Programm cannot read/open file because he dont exist";
        break;
    case cannot_open_file:
        cout<<"Programm cannot open file";
        break;
    case unmatched_opened_comment:
        cout<<"Finded unmatched opened comment.";
        break;
    case unmatched_closed_comment:
        cout<<"Finded unmatched closed comment.";
        break;
    case cannot_write_file:
        cout<<"Programm cannot write file because he dont have permisions.";
        break;
    default:
        break;
    }
}

void print_lines_error(Error error)
{
    print_error(error.Type);
    cout<<"Please check your source file in line " << error.linenumber;
}

