#include "workwithfile.h"


void search_comments(QString text, QVector <QPair<int, int> > &comment_locations)
{
    bool is_comment =false; // флаг, который указывает, на то,  находимся мы внутри однострочного комментария или нет
    bool is_string = false;  //  флаг, который указывает, на то, находимся ли мы внутри строки или нет.
    bool beginning_found = false; // флаг, который  указывает, на то, что найдено начало многострочного комментария.
    int line=0;
    int begin_line = 0;
    for(int i = 0 ; i < text.length() ; i++)
    {
        if(text[i] == '"' || text[i] == '\'')
        {
            if(is_string){
            is_string = false;}
            else{ is_string = true;}
        }
        if(text[i] == '/' && text[i+1] == '/')
        {
            is_comment = true;
        }
        if(text[i] == '\n')
        {
            line++;
            is_comment = false;
        }
        if(text[i] == '/' && text[i+1] == '*')
        {
            if(!beginning_found){
            if(!is_comment && !is_string){
            begin_line = i;
            beginning_found = true;}
            }
        }
        if(text[i] == '*' && text[i+1] == '/')
        {
            if(!is_comment && !is_string){
            QPair<int,int> Pair;
            Pair.first = begin_line;
            Pair.second = i+2;
            comment_locations.append(Pair);
            beginning_found = false;
            }

        }
    }
}

void delete_comments(QString &text, QVector <QPair<int, int> > comment_locations)
{
    for(int i = comment_locations.count()-1; i>-1 ; i--)
    {
        QPair<int,int> Pair;
        Pair = comment_locations[i];
        text.remove(Pair.first,Pair.second-Pair.first);
    }
}

QVector<Error> find_unmached_comments(QString text)
{
    QVector<Error> error_vector;
    bool is_comment = false; // флаг, который указывает, на то,  находимся мы внутри однострочного комментария или нет
    bool is_string = false;  //  флаг, который указывает, на то, находимся ли мы внутри строки или нет.
    bool beginning_found = false; // флаг, который  указывает, на то, что найдено начало многострочного комментария.
    int line=1;
    int begin_line = 0;
    for(int i = 0 ; i < text.length() ; i++)
    {
        if(text[i] == '"')
        {
            if(is_string){
            is_string = false;}
            else{ is_string = true;}
        }
        if(text[i] == '/' && text[i+1] == '/')
        {
            is_comment = true;
        }
        if(text[i] == '\n')
        {
            line++;
            is_comment = false;
        }
        if(text[i] == '/' && text[i+1] == '*')
        {
            if(!beginning_found){
            if(!is_comment && !is_string){
            begin_line = line;
            beginning_found = true;
            }
            }
        }
        if(text[i] == '*' && text[i+1] == '/')
        {
            if(beginning_found == false && !is_string && !is_comment){
                Error newError;
                newError.linenumber = line;
                newError.Type = unmatched_closed_comment;
                error_vector.append(newError);
                return error_vector;
            }
            if(!is_string && !is_comment){
            beginning_found = false;}

        }
    }
    if(beginning_found )
    {
        Error newError;
        newError.linenumber = begin_line;
        newError.Type = unmatched_opened_comment;
        error_vector.append(newError);
        return error_vector;
    }

    return error_vector;
}
