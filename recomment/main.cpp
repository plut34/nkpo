#include <QCoreApplication>
#include "constants.h"
#include "check_functions.h"
#include "workwithfile.h"
#include "inoutput.h"
#include "Tests/test_for_search_comments.h"
#include "Tests/test_for_find_unmached_comments.h"

using namespace std;

int main(int argc, char *argv[])
{
#ifdef QT_DEBUG
    QTest::qExec(new test_for_find_unmached_comments);
    QTest::qExec(new test_for_search_comments);
#endif
    QString text;
    QVector<ErrorType> error_vector;
    QVector<Error> error_struct;
    QVector<QPair<int,int>> comment_locations;
    error_vector = check_arguments(argc,argv);

    if(error_vector.count() != 0)
    {
        print_error(error_vector[0]);
        return 0;
    }

    error_vector = read_file(QString(argv[1]), text);

    if(error_vector.count() != 0)
    {
        print_error(error_vector[0]);
        return 0;
    }

    error_struct = find_unmached_comments(text);

    if(error_struct.count() != 0)
    {
        print_lines_error(error_struct[0]);
        return 0;
    }

    search_comments(text,comment_locations);
    delete_comments(text,comment_locations);

    if(!writeData(text,QString(argv[2])))
    {
        print_error(cannot_write_file);
    }

    return 0;
}
