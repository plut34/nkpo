#ifndef TEST_FOR_SEARCH_COMMENTS_H
#define TEST_FOR_SEARCH_COMMENTS_H
#pragma once

#include <QObject>
#include <QTest>
#include <QMap>
#include <QString>
#include <QList>
#include <QDebug>
#include <workwithfile.h>

class test_for_search_comments : public QObject
{
    Q_OBJECT
public:
    explicit test_for_search_comments(QObject *parent = 0);

signals:

public slots:

private Q_SLOTS:
    void test_data();
    void test();
};

#endif // TEST_FOR_SEARCH_COMMENTS_H
