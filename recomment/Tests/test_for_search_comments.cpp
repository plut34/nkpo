#include "test_for_search_comments.h"
typedef QVector<QPair<int, int>> PairVector;
Q_DECLARE_METATYPE(PairVector)

test_for_search_comments::test_for_search_comments(QObject *parent) : QObject(parent)
{

}

void test_for_search_comments::test_data()
{
    QString text;
    QVector<QPair<int, int>>  result;

    QTest::addColumn< QString > ("text");
    QTest::addColumn<  QVector<QPair<int, int> > >("result");

    text = "/*\nФункция производящая сложение двух чисел\n*/\nint sum(int a, int b)\n{\nreturn a+b;\n}\n";
    QPair<int,int> Pair;
    Pair.first = 0;
    Pair.second = 46;
    result.append(Pair);

    QTest::newRow("Один многострочный комментарий в тексте программы") << text <<result;

    text.clear();
    result.clear();
    text = "/*главная функция*/\nvoid main()\n{\n//Объявление переменных\nint a, b;\n/*\nВвод чисел\n*/\nscanf(“%d”, &a);\nscanf(“%d”, &b);\nreturn a+b;\n}\n";
    Pair.first = 0;
    Pair.second = 19;
    result.append(Pair);
    Pair.first = 68;
    Pair.second = 84;
    result.append(Pair);

    QTest::newRow("Несколько комментариев в тексте программы") << text <<result;

    text.clear();
    result.clear();

    text = "///*Функция производящая сложение двух чисел*/\nint sum(int a, int b)\n{\nreturn a+b;\n}\n";

    QTest::newRow("Многострочный комментарий содержится внутри однострочного комментария") << text <<result;


    text.clear();
    result.clear();

    text = "void main()\n{\nint a, b;\nscanf(“%d”, &a);\nscanf(“%d”, &b);\nif(a>b)\n{\nprintf("/*a>b*/");\n}\n}\n";

    QTest::newRow("Многострочный комментарий находится внутри строки") << text <<result;

    text.clear();
    result.clear();

    text = "void main()\n{\nchar constanta='/*Polozov A. S.*/';\nprintf( &constanta);\n}\n";

    QTest::newRow("Многострочный комментарий содержится внутри константы") << text <<result;

    text.clear();
    result.clear();

    text = "void main()\n{\nint a, b;\nscanf(“%d”, &a);\nscanf(“%d”, &b);{ // /*Вывести сообщение\nprintf(\"*/\");\n}\n}\n";

    QTest::newRow("Начало многострочного комментария закомментировано, а конец является частью строки") << text <<result;

    text.clear();
    result.clear();

    text = "/*\nФункция производящая сложение /* двух чисел\n*/\nint sum(int a, int b)\n{\nreturn a+b;\n}\n";
    Pair.first = 0;
    Pair.second = 49;
    result.append(Pair);

    QTest::newRow("Внутри многострочного комментария находится начало /* многострочного комментария") << text <<result;

    text.clear();
    result.clear();

    text = "/*\nФункция производящая сло/*жение /* двух чисел/*\n*/\nint sum(int a, int b)\n{\nreturn a+b;\n}\n";
    Pair.first = 0;
    Pair.second = 53;
    result.append(Pair);

    QTest::newRow("Внутри многострочного комментария находится несколько начал /* многострочных комментариев") << text <<result;

    text.clear();
    result.clear();

    text = "void main()\n{\nint a, b;\n/*Ввод чисел*/\nscanf(“%d”, &a);\nscanf(“%d”, &b);\nif(a>b)\n{///*Вывести сообщение*/\nprintf("/*a>b*/");\n}\n}\n";
    Pair.first = 24;
    Pair.second = 38;
    result.append(Pair);

    QTest::newRow("В тексте программы содержится несколько комментариев, часть из них находится внутри однострочного комментария, а часть в строке") << text <<result;
}

void test_for_search_comments::test()
{


    QVector<QPair<int, int>>  result_function;
    QFETCH(QString, text);
    QFETCH(PairVector, result);

    search_comments(text, result_function);

    QVERIFY2(result_function == result, "INVALID RESULT");
}

