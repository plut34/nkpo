#include "inoutput.h"

QVector<ErrorType> read_file(QString filename, QString &text)
{
    QVector<ErrorType> error_vector;
    QString str;
    QFile file(filename);
    if (file.exists())
    {
        if (file.open(QIODevice::ReadOnly))
        {
            str = file.readLine();
            while(!str.isEmpty())
            {
                text.append(str);
                str =file.readLine();
            }
        }
        else{
            error_vector.append(cannot_read_file);
            return error_vector;
        }
        file.close();
    }
    else{
        error_vector.append(file_dont_exists);
        return error_vector;

    }

    return error_vector;
}

bool writeData (QString text, QString newfilename)
{
    QFile file(newfilename);

    if (file.open(QIODevice::WriteOnly | QIODevice::Truncate))
    {
        QTextStream output(&file);

        output << text;

        file.close();
        return true;
    }

    return false;
}
