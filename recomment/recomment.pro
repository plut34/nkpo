QT += core testlib
QT -= gui

CONFIG += c++11

TARGET = recomment
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    check_functions.cpp \
    inoutput.cpp \
    workwithfile.cpp \
    Tests/test_for_search_comments.cpp \
    Tests/test_for_find_unmached_comments.cpp

HEADERS += \
    check_functions.h \
    constants.h \
    inoutput.h \
    workwithfile.h \
    Tests/test_for_search_comments.h \
    Tests/test_for_find_unmached_comments.h
