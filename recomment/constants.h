#ifndef CONSTANTS
#define CONSTANTS
#include <QPair>
#include <QVector>
#include <QString>
#include <QFile>
#include <iostream>
#include <string>
#include <QTextStream>
using namespace std;

/*!
 * \brief ErrorType перечисление содержащие возможные типы ошибок
 */
enum ErrorType
{
    wrong_count_of_arg,
    not_in_arg,
    not_out_arg,
    cannot_read_file,
    file_dont_exists,
    cannot_open_file,
    cannot_write_file,
    unmatched_opened_comment,
    unmatched_closed_comment
};

/*!
 * \brief Error структура содержащая тип ошибки и строку в которой она произошла
 */
struct Error
{
    ErrorType Type;
    int linenumber;

    bool operator==(const Error &other) const
    {
        if(Type==other.Type && linenumber==other.linenumber)
            {
                return true;
            }
            return false;

    }

};

#endif // CONSTANTS

