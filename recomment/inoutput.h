#ifndef INOUTPUT_H
#define INOUTPUT_H
#include "constants.h"

/*!
 * \brief read_file функция чтения входного файла с исходныи кодом программы
 * \param[in] filename - название входного файла
 * \param[out] text - строка содержащая исходный код из входного файла
 * \return QVector<ErrorType> - типы ошибок при чтении из файла
 */
QVector<ErrorType> read_file(QString filename, QString &text);

/*!
 * \brief writeData функция записи результата работы программы в файл
 * \param[in] text - строка содержащая измененный код программы
 * \param[in] newfilename - имя файла в который производится запись
 * \return bool - результат записи в файл, true - если успешно, false - если нет
 */
bool writeData (QString text, QString newfilename);

#endif // INOUTPUT_H
